#!/usr/bin/php

<?php

/**
 * This is a php exercice meant to be executed by a command line. 
 * 
 * Your code goes in the two following methods.
 *  
 * Implement a function that parses a ini file and generates a corresponding array. 
 * Ini files can have depth folders specified by brackets.
 */
function toArrayWithPhpNativeMethod(string $iniFileName) : array {
    // ...Let's do it the proper way ... with parse_ini_file
    $array = [];
    return $array;
}

function toArrayTheHardcodedWay(string $iniFileName) : array {
    // ... But let's not forget that our main goal is to practice algorithms... so implement it only with fopen
    $array = [];
    return $array;
}


/***
 * The linked tests
 */

// Reading arguments given by command line. Check this via : var_dump($argv);
$scriptArgument = $argv[1];



// Exercice 1 : Testing the proper way to implement it.
$array = toArrayWithPhpNativeMethod($scriptArgument);
assert(array_key_exists("database", $array), "\033[31m KO -- missing 'database' key in result array. \n");
assert($array["database"]["port"] == 143);
assert($array["database"]["file"] == "payroll.dat");
assert(!array_key_exists("author", $array), "\033[31m KO -- comment is not taken into account. \n");

// Exercice 2 : Testing with hardcoded way, looping on brackets and stuff... 
$array_hardcoded = toArrayTheHardcodedWay($scriptArgument);
assert(array_key_exists("owner", $array_hardcoded), "\033[31m KO -- missing 'owner' key in result array. \n");
assert($array_hardcoded["owner"]["name"] == "Mr. Patrick OConnell");
assert($array_hardcoded["owner"]["organization"] == "Titanic Inc.");
assert(!array_key_exists("author", $array_hardcoded), "\033[31m KO -- comment is not taken into account. \n");

// Handy message to know that every went as expected.

echo "\033[32m OK\n";