#!/usr/bin/php

<?php

/**
 * This is a php exercice meant to be executed by a command line. 
 * 
 * Your code goes in the two following methods.
 *  
 * Implement a function that parses a env file and stores variables in environment
 */
function readEnvFileProperly(string $envFilepath = ".env") : void {
    // ... Please use the DotEnv module : https://github.com/vlucas/phpdotenv
}

function readEnvFileTheHardCodedWay(string $envFilepath = ".env") : void {
    // ... Let's not forget that our main goal is to practice algorithms... so implement it only with fopen and putenv and getenv
    if (is_file($envFilepath)) {
        $lines = file($envFilepath, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        
    }
}


/***
 * The linked tests
 */

// Reading arguments given by command line. Check this via : var_dump($argv);
$scriptArgument = $argv[1] ?? ".env";



// Exercice 1 : Testing with hardcoded way, looping on brackets and stuff... 
$array_hardcoded = readEnvFileTheHardCodedWay($scriptArgument);
assert($_ENV["MAIL_FROM_ADDRESS"] == "salut.patrick@servername.local");
assert(getenv("APP_NAME") == "TitanicApplication");

// Reset
putenv('MAIL_FROM_ADDRESS=');
putenv('APP_NAME=');

// Exercice 2 : Testing the proper way to implement it.
$array = readEnvFileProperly($scriptArgument);
assert($_ENV["MAIL_FROM_ADDRESS"] == "salut.patrick@servername.local");
assert(getenv("APP_NAME") == "TitanicApplication");



// Handy message to know that every went as expected.

echo "\033[32m OK\n";