#!/usr/bin/php

<?php

/**
 * This is a php exercice meant to be executed by a command line. 
 * 
 * Your code goes in the two following methods.
 *  
 * Implement a function that parses a yaml file and stores the data ion an array
 */
function readYamlFileProperly(string $yamlFilepath) : array {
    // ... Please use the yaml_parse or yaml_parse_file method
    return [];
}

function readYamlFileTheHardCodedWay(string $yamlFilepath ) : array {
    // ... Let's not forget that our main goal is to practice algorithms... so implement it only with fopen and putenv and getenv
    if (is_file($yamlFilepath)) {
        $lines = file($yamlFilepath, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        
    }
    return [];
}


/***
 * The linked tests
 */

// Reading arguments given by command line. Check this via : var_dump($argv);
$scriptArgument = $argv[1] ?? ".env";


// Exercice 1 : Testing the proper way to implement it.
$array = readYamlFileProperly($scriptArgument);
assert($array['version'] == "3.7");
assert($array['services']['app']['image'] == "titanic-app/test_jpa:v0.0.1");


// Exercice 2 : Testing with hardcoded way, looping on brackets and stuff... 
$array_hardcoded = readYamlFileTheHardCodedWay($scriptArgument);
assert(count($array_hardcoded) == 3);
assert($array_hardcoded['services']['db']['image'] == "postgres:13.3-alpine");
assert(! array_key_exists('links', $array_hardcoded['services']['app']));




// Handy message to know that every went as expected.

echo "\033[32m OK\n";