#!/usr/bin/php

<?php

/**
 * This is a php exercice meant to be executed by a command line. 
 * 
 * Your code goes in the two following methods.
 *  
 * Implement a function that parses a csv file and generates a corresponding array. 
 * The given CSV file has a first line as field headers.
 */

function readCSVFile(string $csvFilepath) : array {
    // ... Let's not forget that our main goal is to practice algorithms... so implement it only with fopen and fgetcsv or str_getcsv
    $csv_data = [];
    return $csv_data;
}



/***
 * The linked tests
 */

// Reading arguments given by command line. Check this via : var_dump($argv);
$scriptArgument = $argv[1];



// Exercice 1 : the proper way to implement it... 
$array = readCSVFile($scriptArgument);
assert(count($array) == 891);
assert($array[629]['Name'] == "OConnell, Mr. Patrick D");


echo "\033[32m OK\n";