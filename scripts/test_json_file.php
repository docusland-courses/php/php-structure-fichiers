#!/usr/bin/php

<?php

/**
 * This is a php exercice meant to be executed by a command line. 
 * 
 * Your code goes in the two following methods.
 *  
 * Implement a function that parses a json file and generates a corresponding array.
 */

function readJsonFile(string $jsonFilepath) : array {
    // ... The proper way, nowadays, is to use json_decode. Careful, here we expected an array returned... not an object
    $json_data = [];
    return $json_data;
}

function readJsonTheHardCodedWay(string $jsonFilepath) : array {
    // ... Let's not forget that our main goal is to practice algorithms... so implement it only loops and regex
    if (is_file($jsonFilepath)) {
        $lines = file($jsonFilepath, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        
    }
    return $lines;
}

/***
 * The linked tests
 */

// Reading arguments given by command line. Check this via : var_dump($argv);
$scriptArgument = $argv[1];



// Exercice 1 : the proper way to implement it... 
$array = readJsonFile($scriptArgument);
assert(count($array) == 6);
assert($array['name'] == "Titanic");
assert($array['scripts']['dev'] == "vite");



// Exercice 2 : the unrecommended way to do it, but damn hard algorithm here...

$array = readJsonTheHardCodedWay($scriptArgument);
assert(count($array) == 6);
assert($array['private']);
assert($array['dependencies']['roboto-fontface'] == "*");



echo "\033[32m OK\n";