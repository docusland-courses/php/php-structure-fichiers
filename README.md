# Fichiers et Algo PHP

En informatique, il existe de nombreux types de fichiers plats. 
Ces fichiers textuels se basent sur leur extension pour spécifier leur format. Suite à celà il est convenu aux développeurs d'écrire ou de générer des fichiers respectant ces conventions. 

Au sein de ce dépôt, vous trouverez des fichiers "classiques".
Entrainez-vous à implémenter des fonctions qui parcourent ses fichiers et récupèrent de la donnée à partir de ces fichiers. 
Systématiquement, il vous sera demandé d'ouvrir les fichiers en utilisant les méthodes attendues en PHP mais aussi de les manipuler sans aucune bibliothèque. Et donc de les manipuler à la sueur de votre front.

Avant de pouvoir réaliser les exercices, assurez-vous que les assertions sont activées au sein de votre php
```shell
php -i | grep zend.assertions
```

Si ce n'est pas le cas, un fichier php.ini a été mis à disposition dans ce dossier pour faciliter les tests. 
Ainsi, lors de l'execution de vos tests en ligne de commande, ajoutez l'argument : 
`-c php.ini`.

## Fichier INI
 - [Documentation sur le fichier ini](https://fr.wikipedia.org/wiki/Fichier_INI#Description_du_format)

Au sein de ce dépôt, se trouve le fichier `./files/config.ini`.
Tout d'abord ouvrez-le afin de voir à quoi ressemble ce type de fichier, puis implémentez le test suivant : 

```shell
php scripts/test_ini_file.php files/config.ini
``` 
ou

```shell
php -c php.ini scripts/test_ini_file.php files/config.ini
```

L'ensemble du code que vous devez produire se réalisera au sein du fichier `scripts/test_ini_file.php`.


## Fichier ENV
- [Les variables d'environnement](https://fr.wikipedia.org/wiki/Variable_d%27environnement)

Au sein de ce dépôt, se trouve le fichier `./files/.env`.
Ce type de fichier est très fréquemment utilisé par les développeurs. Il permets d'y définir des variables d'environnements. Et ainsi de définir des variables globales pour toute l'application.

Chaque ligne dans ce fichier définit une variable et sa valeur. 

Tout d'abord ouvrez-le afin de voir à quoi ressemble ce type de fichier, puis implémentez le test suivant : 

```shell
php scripts/test_env_file.php files/.env
``` 
ou

```shell
php -c php.ini scripts/test_env_file.php files/.env
```
L'ensemble du code que vous devez produire se réalisera au sein du fichier `scripts/test_env_file.php`.

## Fichier CSV
 - [Lien Wikipedia sur le CSV](https://en.wikipedia.org/wiki/Comma-separated_values)

Au sein de ce dépôt, se trouve le fichier `./files/data_titanic.csv`.
Tout d'abord ouvrez-le afin de voir à quoi ressemble ce type de fichier, puis implémentez le test suivant : 

```shell
php scripts/test_csv_file.php files/data_titanic.csv
``` 
ou

```shell
php -c php.ini scripts/test_csv_file.php files/data_titanic.csv
```
Attention, malgré son nom "Comma Serapated Values", nous les français n'utilisons pas la virgule comme séparateur... Mais le point-virgule.
L'ensemble du code que vous devez produire se réalisera au sein du fichier `scripts/test_csv_file.php`.

## Fichier JSON
 - [Lien Wikipedia](https://fr.wikipedia.org/wiki/JavaScript_Object_Notation)

Au sein de ce dépôt, se trouve le fichier `./files/package.json`.
Tout d'abord ouvrez-le afin de voir à quoi ressemble ce type de fichier. Ce type de fichier est très classique aujourd'hui. 
Il s'agit du format des objet lorsqu'on manipule des api, de fichiers de config js ou autre... Comme son nom l'indique `Javascript Object Notation`, ce format est particulièrement utilisé en JS. 

Implémentez le test suivant : 

```shell
php scripts/test_json_file.php files/package.json
``` 
ou

```shell
php -c php.ini scripts/test_json_file.php files/package.json
```
L'ensemble du code que vous devez produire se réalisera au sein du fichier `scripts/test_json_file.php`.

## Fichier YAML
 - [Documentation](https://yaml.org/)
 
Finalement, au sein de ce dépôt se trouve le fichier `./files/docker-compose.yml`.
Tout d'abord ouvrez-le afin de voir à quoi ressemble ce type de fichier. Ce type de fichier est très classique aujourd'hui également.
Ici il s'agit d'un fichier docker permettant de lancer un conteneur postgre et java.


Implémentez le test suivant : 

```shell
php scripts/test_yaml_file.php files/docker-compose.yml
``` 
ou

```shell
php -c php.ini scripts/test_yaml_file.php files/docker-compose.yml
```
L'ensemble du code que vous devez produire se réalisera au sein du fichier `scripts/test_yaml_file.php`.